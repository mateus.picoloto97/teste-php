<?php

class Change {

    private $oneReal;
    private $fiftyCentavos;
    private $twentyFiveCentavos;
    private $tenCentavos;
    private $fiveCentavos;
    private $oneCentavo;

    public function Change() {
        $this->oneReal = 0;
        $this->fiftyCentavos = 0;
        $this->twentyFiveCentavos = 0;
        $this->tenCentavos = 0;
        $this->fiveCentavos = 0;
        $this->oneCentavo = 0;
    }

    public function returnChange(float $totalValue, float $valueRecieved) {
        if ( $totalValue > 0 && $valueRecieved > 0 &&  $totalValue < $valueRecieved ) {
            $diffence = $valueRecieved - $totalValue;
            
            $this->oneReal = (int) $diffence / 1;
            $diffence = fmod($diffence, 1);

            $this->fiftyCentavos = (int) ($diffence / 0.5);
            $diffence = fmod($diffence, 0.5);

            $this->twentyFiveCentavos = (int) ($diffence / 0.25);
            $diffence = fmod($diffence, 0.25);

            $this->tenCentavos = (int) ($diffence / 0.1);
            $diffence = fmod($diffence, 0.1);

            $this->fiveCentavos = (int) ($diffence / 0.05);
            $diffence = fmod($diffence, 0.05);

            $this->oneCentavo = (int) ($diffence / 0.01);
            $diffence = fmod($diffence, 0.01);

            $out = "";

            if ( $this->oneReal > 0 ) {
                $out .= $this->oneReal . (( $this->oneReal == 1 ) ? ' moeda ' : ' moedas ') . ' de 1 real; ';
            }
            if ( $this->fiftyCentavos > 0 ) {
                $out .= $this->fiftyCentavos . (( $this->fiftyCentavos == 1 ) ? ' moeda ' : ' moedas ') . ' de 50 centavos; ';
            }
            if ( $this->twentyFiveCentavos > 0 ) {
                $out .= $this->twentyFiveCentavos . (( $this->twentyFiveCentavos == 1 ) ? ' moeda ' : ' moedas ') . ' de 25 centavos; ';
            }
            if ( $this->tenCentavos > 0 ) {
                $out .= $this->tenCentavos . (( $this->tenCentavos == 1 ) ? ' moeda ' : ' moedas ') . ' de 10 centavos; ';
            }
            if ( $this->fiveCentavos > 0 ) {
                $out .= $this->fiveCentavos . (( $this->fiveCentavos == 1 ) ? ' moeda ' : ' moedas ') . ' de 5 centavos; ';
            }
            if ( $this->oneCentavo > 0 ) {
                $out .= $this->oneCentavo . (( $this->oneCentavo == 1 ) ? ' moeda ' : ' moedas ') . ' de 1 centavo; ';
            }

            $out = trim($out);

            echo $out;
        }
    }

}

?>