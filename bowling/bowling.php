<?php

require_once 'frame.php';

class Bowling {
    
    private $frames;

    public function Bowling() {
        $this->frames = [];
    }

    public function roll (int $pins) {
        if ( -1 < $pins &&  $pins < 11 ) {
            if ( count($this->frames) === 0 ) {
                $frame = new Frame();
                $frame->setPlays([$pins]);
                $this->frames[] = $frame;
            } else if ( count($this->frames) === 10 ) {
                end($this->frames);
                $key = key($this->frames);
                if ( $this->frames[$key]->getPlays()[0] === 10 ) {
                    if ( count($this->frames[$key]->getPlays()) < 3 ) {
                        $plays = $this->frames[$key]->getPlays();
                        $plays[] = $pins;
                        $this->frames[$key]->setPlays($plays);
                    }
                } else if ( $this->isSpare($key) ) {
                    if ( count($this->frames[$key]->getplays()) < 3 ) {
                        $plays = $this->frames[$key]->getPlays();
                        $plays[] = $pins;
                        $this->frames[$key]->setPlays($plays);
                    }
                } else {
                    if ( count($this->frames[$key]->getPlays()) < 2 ) {
                        $plays = $this->frames[$key]->getPlays();
                        $plays[] = $pins;
                        $this->frames[$key]->setPlays($plays);
                    }
                }
            } else {
                end($this->frames);
                $key = key($this->frames);
                if ( count($this->frames[$key]->getPlays()) < 2 ) {
                    if ( isset($this->frames[$key]->getPlays()[0]) && $this->frames[$key]->getPlays()[0] === 10 ) {
                        $frame = new Frame();
                        $frame->setPlays([$pins]);
                        $this->frames[] = $frame;
                    } else {
                        if ( $pins < 10 ) {
                            $plays = $this->frames[$key]->getPlays();
                            $plays[] = $pins;
                            $this->frames[$key]->setPlays($plays);
                        }
                    }
                } else {
                    $frame = new Frame();
                    $frame->setPlays([$pins]);
                    $this->frames[] = $frame;
                }
            }
        }
    }

    public function score() {
        $score = 0;
        foreach ( $this->frames as $i => $frame ) {
            foreach ($frame->getPlays() as $j => $play) {
                if ( $j === 0 && $play === 10 ) {
                    $count = 0;
                    if ( isset($this->frames[$i + 1]) && $this->frames[$i + 1]->getPlays() != null && count($this->frames[$i + 1]->getPlays()) > 0 ) {
                        foreach ( $this->frames[$i + 1]->getPlays() as $value ) {
                            $score += $value;
                            ++$count;
                        }
                    }
                    if ( $count < 2 && isset($this->frames[$i + 2]) && $this->frames[$i + 2]->getPlays() != null && count($this->frames[$i + 2]->getPlays()) > 0 ) {
                        $score += $this->frames[$i + 2]->getPlays()[0];
                    }
                    $score += $play;
                } else if ( $this->isSpare($i) ) {
                    if ( isset($this->frames[$i + 1]) && $this->frames[$i + 1]->getPlays() != null && count ($this->frames[$i + 1]->getPlays()) > 0 ) {
                        $score += $this->frames[$i + 1]->getPlays()[0];        
                    }
                    $score += $this->frames[$i]->getPlays()[0] + $this->frames[$i]->getPlays()[1];
                    break;
                } else {
                    $score += $play;
                }
            }
        }
        return $score;
    }

    private function isSpare(int $i) {
        return (isset($this->frames[$i]->getPlays()[0]) && isset($this->frames[$i]->getPlays()[1]) && ($this->frames[$i]->getPlays()[0] + $this->frames[$i]->getPlays()[1]) === 10);
    }

}

?>