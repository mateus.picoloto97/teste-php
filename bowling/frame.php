<?php

class Frame {
    private $plays;

    public function Frame() {
        $this->plays = [];
    }

    public function getPlays() {
        return $this->plays;
    }

    public function setPlays(array $plays) {
        $this->plays = $plays;
    }

}

?>